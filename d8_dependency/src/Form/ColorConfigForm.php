<?php

namespace Drupal\d8_dependency\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Color Config.
 */
class ColorConfigForm extends ConfigFormBase {
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   *
   */
  protected function getEditableConfigNames(): array {
    return [
      'd8_dependency.settings',
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'color_config_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('d8_dependency.settings')->get();
    $form['color_config'] = [
      '#type' => 'textarea',
      '#title' => t('Color Codes'),
      '#default_value' => !empty($config['color_config']) ? $config['color_config'] : '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
   $values = $form_state->getValues();
   $config = $this->configFactory->getEditable('d8_dependency.settings');
   $config->set('color_config', $values['color_config']);
   $config->save();

   drupal_set_message(t("The configurations have been saved."));
  }

}
